var indexSectionsWithContent =
{
  0: "abcdfghimnoprstuvw",
  1: "abfpu",
  2: "abfrtu",
  3: "abcdfgmrst",
  4: "abcdfhmnoprtvw",
  5: "dpt",
  6: "dhpu",
  7: "i"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "defines",
  7: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Macros",
  7: "Pages"
};

