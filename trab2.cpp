#include "banco.h"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <stdio.h>

using namespace std;
using namespace cv;

string face_cascade_name = "haarcascade_frontalface_alt.xml";
CascadeClassifier face_cascade;
string window_name = "Capture - Face detection";
int filenumber; // Number of file to be saved
string filename;
extern int vetor[20];
int opcao;
string nome;
unsigned long int cpf;

//criacao de classes para os funcionarios (podem fazer reserva)

class FuncionarioSeguranca : public Funcionarios{} ;

class FuncionarioLimpeza : public Funcionarios{} ;

class FuncionarioCic : public Funcionarios{} ;

class FuncionarioExterno : public Funcionarios{} ;

class Professor : public Funcionarios, public Auxiliar{} ;

//declarando funcoes do arquivo
//Funcao para evitar problemas ao fazer uma reserva
void detectAndDisplay(Mat frame);
void setVetorToNull();
void cadastrarAluno();
void cadastrarFuncionario();
void cadastrar();
void fazerReserva();
void remover();
void adicDisciplina();
void removerDisciplina();
void acessarLinf();
int foto();
string toUP(string s);

Banco b;

int main(){
	 system("mkdir img/users/");
	b.start();

	do{
		system("clear");
		cout << "\n------------------------";
		cout << "\n--BEM-VINDO(A) AO LINF--";
		cout << "\n------------------------";
		cout << "\n\nSelecione qual opcao deseja realizar: ";
		cout << "\n1 - CADASTRAR";
		cout << "\n2 - FAZER RESERVA";
		cout << "\n3 - REMOVER";
		cout << "\n4 - ADICIONAR DISCIPLINA";
		cout << "\n5 - REMOVER DISCIPLINA";
		cout << "\n6 - ACESSAR LINF";
		cout << "\n0 - SAIR";
		cout << "\nOPCAO: ";
		cin >> opcao;
	// Repete até que o usuário peça para sair.
		switch (opcao)
		{
			case 1:
			cadastrar();
			opcao = 1;
			break;
			case 2:
			fazerReserva();
			opcao = 2;
			break;
			case 3:
			remover();
			opcao = 3;
			break;
			case 4:
			adicDisciplina();
			opcao = 4;
			break;
			case 5:
			removerDisciplina();
			opcao = 5;
			break;
			case 6:
			acessarLinf();
			opcao = 6;
			break;
			case 0:
			break;
			default:
		    // Se o usuário informar um valor inválido.
			cout << "Opcao invalida! Informe outro valor." << endl;
			break;
		}
	}while (opcao != 0);

	b.close();
	return 0;
}

void setVetorToNull(){
	for(int i=0; i<20; i++){
		vetor[i] = 0;
	}
}

void cadastrarAluno(){
	unsigned long int matricula;
	Aluno a;
	system("clear");
	cout << "\n---------------------";
	cout << "\n--CADASTRO DE ALUNO--";
	cout << "\n---------------------";
	cin.ignore();
	cout << "\nINFORME O NOME COMPLETO: ";
	getline(cin, nome);
	nome = toUP(nome);
	a.setNome(nome);
	cout << "\nINFORME A MATRICULA: ";
	cin >> matricula;
	a.setMatricula(matricula);
	cin.ignore();
	cout << "\nINFORME O CPF: ";
	cin >> cpf;
  foto();
  a.setCpf(cpf);
  b.addToBD(&a);
}

void cadastrarFuncionario(){
	Funcionarios f;
	system("clear");
	cout << "\n---------------------------";
	cout << "\n--CADASTRO DE FUNCIONARIO--";
	cout << "\n---------------------------";
	cin.ignore();
	cout << "\nINFORME O NOME COMPLETO: ";
	getline(cin, nome);
	nome = toUP(nome);
	f.setNome(nome);
	cout << "\nINFORME O CPF: ";
	cin >> cpf;
  foto();
  f.setCpf(cpf);
  b.addToBD(&f);
}

int foto()
{
   VideoCapture capture(0);

    if (!capture.isOpened())  // check if we succeeded
      return -1;

    // Load the cascade
    if (!face_cascade.load(face_cascade_name))
    {
      printf("--(!)Error loading\n");
      return (-1);
    };

    // Read the video stream
    Mat frame;

    for (int i=0;i<10;i++)
    {
      capture >> frame;

        // Apply the classifier to the frame
      if (!frame.empty())
      {
        detectAndDisplay(frame);
      }
      else
      {
        printf(" --(!) No captured frame -- Break!");
        break;
      }

      int c = waitKey(10);

      if (27 == char(c))
      {
        break;
      }
    }

    return 0;
  }
  void cadastrar(){
   do{
    system("clear");
    cout << "\n------------";
    cout << "\n--CADASTRO--";
    cout << "\n------------";
    cout << "\nO QUE DESEJA CADASTRAR?";
    cout << "\n1 - ALUNO";
    cout << "\n2 - FUNCIONARIO";
    cout << "\n0 - VOLTAR";
    cout << "\nOPCAO: ";
    cin >> opcao;
    switch (opcao){
     case 1:
     cadastrarAluno();
     opcao = 0;
     break;
     case 2:
     cadastrarFuncionario();
     opcao = 0;
     break;
     case 0:
     break;
     default:
				    // Se o usuário informar um valor inválido.
     cout << "Opcao invalida! Informe outro valor.-" << endl;
     break;
   }
 }while (opcao != 0);
}

void fazerReserva(){
 string mot;
 int sala;
 Funcionarios f;
 system("clear");
 cout << "\n-----------------";
 cout << "\n--FAZER RESERVA--";
 cout << "\n-----------------";
 cout << "\nQUAL TIPO DE FUNCIONARIO DESEJA FAZER A RESERVA?";
 cout << "\n1 - PROFESSOR";
 cout << "\n2 - DEMAIS FUNCIONARIOS";
 cout << "\nOPCAO: ";
 cin >> opcao;
 cin.ignore();
 cout << "\nINFORME O CPF: ";
 cin >> cpf;
 cin.ignore();
 cout << "\nDIGITE MOTIVO DA RESERVA ";
 cout << "\nMOTIVO: ";
 getline(cin, mot);
 cout << "\nINFORME O NUMERO DA SALA: ";
 cin >> sala;
 setVetorToNull();
 if(opcao == 1){
  f.fazer_reserva(sala, false);
  b.reserva(cpf, true, mot, NULL);
}
else{
  f.fazer_reserva(sala, true);
  b.reserva(cpf, false, mot, &f);
}
}

void remover(){
 system("clear");
 cout << "\n-----------";
 cout << "\n--REMOVER--";
 cout << "\n-----------";
 cin.ignore();
 cout << "\nINFORME O CPF: ";
 cin >> cpf;
 b.rmvFromBD(cpf);
}

void adicDisciplina(){
 Auxiliar a;
 string disc,turm;
 system("clear");
 cout << "\n------------------------";
 cout << "\n--ADICIONAR DISCIPLINA--";
 cout << "\n------------------------";
 cin.ignore();
 cout << "\nINFORME O CPF: ";
 cin >> cpf;
 cin.ignore();
 cout << "\nDIGITE O NOME COMPLETO DA DISCIPLINA: ";
 getline(cin, disc);
 string aux = toUP(disc);
 disc = aux;
 cout << "\nDIGITE A TURMA DA DISCIPLINA: ";
 getline(cin, turm);
 aux = toUP(turm);
 turm = aux;
 a.addDisciplina(disc, turm);
 b.addDiscToBD(&a, cpf);
}

void removerDisciplina(){
 string disc,turm;
 system("clear");
 cout << "\n----------------------";
 cout << "\n--REMOVER DISCIPLINA--";
 cout << "\n----------------------";
 cin.ignore();
 cout << "\nINFORME O CPF: ";
 cin >> cpf;

 b.rmvDisc(cpf);
}

void acessarLinf(){
 system("clear");
 cin.ignore();
 cout << "\n----------------";
 cout << "\n--ACESSAR LINF--";
 cout << "\n----------------";
 cout << "\n1 - PROFESSOR";
 cout << "\n2 - PESSOA EXTERNA";
 cout << "\n3 - ALUNO";
 cout << "\n4 - DEMAIS FUNCIONARIOS";
 cout << "\nOPCAO: ";
 cin >> opcao;
 cout << "\nINFORME O CPF: ";
 cin >> cpf;
 switch (opcao){
  case 1:
  b.find_way(cpf, true, false);
  break;
  case 2:
  b.find_way(cpf, false, true);
  break;
  case 3:
  b.find_way(cpf, false, false);
  break;
  case 4:
  b.find_way(cpf, false, false);
  break;
}
}

string toUP(string s){
 int i =0;

 while(s[i]){
  s[i] = toupper(s[i]);
  i++;
}

return s;
}

void detectAndDisplay(Mat frame)
{
  std::vector<Rect> faces;
  Mat frame_gray;
  Mat crop;
  Mat res;
  Mat gray;
  string text;
  stringstream sstm;
  cvtColor(frame, frame_gray, COLOR_BGR2GRAY);
  equalizeHist(frame_gray, frame_gray);

// Detect faces
  face_cascade.detectMultiScale(frame_gray, faces, 1.1, 2, 0 | CASCADE_SCALE_IMAGE, Size(30, 30));

// Set Region of Interest
  cv::Rect roi_b;
  cv::Rect roi_c;

    size_t ic = 0; // ic is index of current element
    int ac = 0; // ac is area of current element

    size_t ib = 0; // ib is index of biggest element
    int ab = 0; // ab is area of biggest element
    for (ic = 0; ic < faces.size(); ic++) // Iterate through all current elements (detected faces)

    {
      roi_c.x = faces[ic].x;
      roi_c.y = faces[ic].y;
      roi_c.width = (faces[ic].width);
      roi_c.height = (faces[ic].height);

        ac = roi_c.width * roi_c.height; // Get the area of current element (detected face)

        roi_b.x = faces[ib].x;
        roi_b.y = faces[ib].y;
        roi_b.width = (faces[ib].width);
        roi_b.height = (faces[ib].height);

        ab = roi_b.width * roi_b.height; // Get the area of biggest element, at beginning it is same as "current" element

        if (ac > ab)
        {
          ib = ic;
          roi_b.x = faces[ib].x;
          roi_b.y = faces[ib].y;
          roi_b.width = (faces[ib].width);
          roi_b.height = (faces[ib].height);
        }

        crop = frame(roi_b);
        resize(crop, res, Size(128, 128), 0, 0, INTER_LINEAR); // This will be needed later while saving images
        cvtColor(crop, gray, CV_BGR2GRAY); // Convert cropped image to Grayscale
        // Form a filename
        filename = "";
        stringstream ssfn;
        ssfn << "img/users/"<< nome << filenumber << ".png";
        filename = ssfn.str();
        filenumber++;

        imwrite(filename, gray);

        Point pt1(faces[ic].x, faces[ic].y); // Display detected faces on main window - live stream from camera
        Point pt2((faces[ic].x + faces[ic].height), (faces[ic].y + faces[ic].width));
        rectangle(frame, pt1, pt2, Scalar(0, 255, 0), 2, 8, 0);
      }

// Show image
      sstm << "Crop area size: " << roi_b.width << "x" << roi_b.height << " Filename: " << filename;
      text = sstm.str();

      putText(frame, text, cvPoint(30, 30), FONT_HERSHEY_COMPLEX_SMALL, 0.8, cvScalar(0, 0, 255), 1, CV_AA);
      imshow("original", frame);

      if (!crop.empty())
      {
        imshow("detected", crop);
      }
      else
        destroyWindow("detected");
    }