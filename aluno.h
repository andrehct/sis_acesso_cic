#ifndef ALUNO_H
#define ALUNO_H

#include "usuario.h"
#include "auxiliar.h"

class Aluno : public Usuario, public Auxiliar{
private:
    unsigned long int matricula;
public:
	Aluno();
	void setMatricula(unsigned long int x);
	unsigned long int getMatricula();
};

#endif
