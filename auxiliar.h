#ifndef AUXILIAR_H
#define AUXILIAR_H

#include <vector>
#include <string>
using namespace std;

class Auxiliar{
public:
	typedef vector<string> Disciplinas;
	typedef vector<string> Turmas;

	Disciplinas disciplinas;
	Turmas turmas;
	
	void addDisciplina(string s, string c);
};

#endif
