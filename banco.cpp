#include "banco.h"

extern int vetor[20];

#define HOST "localhost" //nome do servidor, por default pode-se colocar localhost
#define USER "root" //qualquer usuario que tem acesso ao banco definido abaixo, por default pode-se colocar root
#define PASS "viniciuss@" //senha para o usuario definido acima
#define DB "Linf" // nome do banco de dados

Banco::Banco(){}

void Banco::start(){

	if(mysql_real_connect(conn, HOST, USER, PASS, DB, 0, NULL, 0))
	{
		cout << "conectado ao banco de dados com sucesso!" << endl;
	
	}
}

void Banco::close(){
	mysql_close(conn);
}


void Banco::addToBD(Funcionarios *f){
	sprintf(comando, "insert into info(cpf,nome) values ('%lu','%s');", f->getCpf(), f->getNome().c_str());
	mysql_query( conn, comando );
}

void Banco::addToBD(Aluno *a){
	sprintf(comando, "insert into info values ('%lu', '%lu', '%s');", a->getCpf(), a->getMatricula(), a->getNome().c_str());
	mysql_query( conn, comando );
}


void Banco::rmvFromBD(unsigned long int x){
	sprintf(comando, "SET FOREIGN_KEY_CHECKS = 0;");
	mysql_query( conn, comando );

	sprintf(comando, "delete from info where cpf = '%lu';", x);
	mysql_query( conn, comando );

	sprintf(comando, "delete from auxtab where FKCpf = '%lu';", x);
	mysql_query( conn, comando );
	
	sprintf(comando, "SET FOREIGN_KEY_CHECKS = 1;");
	mysql_query( conn, comando );
}

void Banco::reserva(unsigned long int x, bool teacher, string motivo, Funcionarios *f){
	int j =1;
	int op, chave;
	vector<int> chaves;
	
	sprintf(comando, "select nome from info where cpf =  '%lu';", x);
	mysql_query( conn, comando );

	res= mysql_store_result(conn);

	row = mysql_fetch_row(res);

	if (row != NULL){  //se o resultado da coluna nao for nulo (ou seja, existe um nome)
		sprintf(comando, "select matricula from info where cpf =  '%lu';", x);
		mysql_query( conn, comando );
		res= mysql_store_result(conn);
		row = mysql_fetch_row(res);

		if( row[0] == NULL) //se o resultado da coluna for nulo (ou seja, nao há matricula)
		{	
			if(teacher){ //se for professor
				sprintf(comando, "select t3.nome, t2.nome, t1.chave from auxtab t1 join turmas t2 on t1.FKturmas = t2.id join disciplinas t3 on t1.FKdisc = t3.cod where t1.FKinfo =  '%lu';", x);
				mysql_query( conn, comando );
				res= mysql_store_result(conn);
				
				if(res == NULL){
					cout<<"professor nao tem disciplinas cadastradas!" << endl;
					return;
				}
				
				int aux = mysql_num_fields(res) ;
				j= 1;

				while ((row = mysql_fetch_row(res)) != NULL ) 
				{ 	
					for (int i=0; i<aux; i++) {
						if(i+1 == aux){ //sempre que chegar no ultimo 'i' será uma chave da tabela auxtab
							int a = atoi(row[i]); //converte o vetor de char para um int
							chaves.push_back(a); //essa chave vai ser salva no vetor de chaves
						}
						else{
							cout << row[i] << " " ;
						}
					}
				
					cout << "      opcao [" << j << "]" <<endl;
					j++;
				} 

				cout << "digite a opcao desejada : ";
				cin >> op;

				chave = chaves.at(op-1);

				if(vetor[2] == 0){ //apenas 1 dia reservado na semana 
					reserva_BD(vetor[7], vetor[8],vetor[9],vetor[10],vetor[0],vetor[5],vetor[1],vetor[19], motivo, chave);
					if(vetor[4] != 0 && vetor[6] != 0){reserva_rec(vetor[7], vetor[8],vetor[9],vetor[10],vetor[0],vetor[5],vetor[1],vetor[19], motivo, chave);}
				}

				if(vetor[2] != 0 && vetor[3] == 0){ //dois dias reservados na semana
					reserva_BD(vetor[7], vetor[8],vetor[9],vetor[10],vetor[0],vetor[5],vetor[1],vetor[19], motivo, chave);
					if(vetor[4] != 0 && vetor[6] != 0){reserva_rec(vetor[7], vetor[8],vetor[9],vetor[10],vetor[0],vetor[5],vetor[1],vetor[19], motivo, chave);}
					reserva_BD(vetor[11], vetor[12],vetor[13],vetor[14],vetor[0],vetor[5],vetor[2],vetor[19], motivo, chave);
					if(vetor[4] != 0 && vetor[6] != 0){reserva_rec(vetor[11], vetor[12],vetor[13],vetor[14],vetor[0],vetor[5],vetor[2],vetor[19], motivo, chave);}
				}
				
				if(vetor[3] != 0){ //tres dias reservados na semana
					reserva_BD(vetor[7], vetor[8],vetor[9],vetor[10],vetor[0],vetor[5],vetor[1],vetor[19], motivo, chave);
					if(vetor[4] != 0 && vetor[6] != 0){reserva_rec(vetor[7], vetor[8],vetor[9],vetor[10],vetor[0],vetor[5],vetor[1],vetor[19], motivo, chave);}
					reserva_BD(vetor[11], vetor[12],vetor[13],vetor[14],vetor[0],vetor[5],vetor[2],vetor[19], motivo, chave);
					if(vetor[4] != 0 && vetor[6] != 0){reserva_rec(vetor[11], vetor[12],vetor[13],vetor[14],vetor[0],vetor[5],vetor[2],vetor[19], motivo, chave);}
					reserva_BD(vetor[15], vetor[16],vetor[17],vetor[18],vetor[0],vetor[5],vetor[3],vetor[19], motivo, chave);
					if(vetor[4] != 0 && vetor[6] != 0){reserva_rec(vetor[15], vetor[16],vetor[17],vetor[18],vetor[0],vetor[5],vetor[3],vetor[19], motivo, chave);}
				}

				
			}


			else{ //se for funcionario(exceto professor)
				Participantes::iterator it ;
				for(it = f->participantes.begin(); it < f->participantes.end() ; it++){
					unsigned long int a = *it;

					sprintf(comando, "select nome from info where cpf = '%lu';", a);
					mysql_query( conn, comando );
					res= mysql_store_result(conn);
					row = mysql_fetch_row(res);

					if(row == NULL){
						cout << "cpf " << a << " nao cadastrado no banco de dados!" << endl;
						cout << "cpf nao sera cadastrado na reserva!" <<endl;
						cout << "Pressione qualquer tecla para continuar" << endl;
						getchar();
						getchar();
						continue;
					}

					sprintf(comando, "insert into auxtab(FKinfo) values ('%lu');", a);
					mysql_query( conn, comando );

					sprintf(comando, "select last_insert_id();");
					mysql_query( conn, comando );
					res= mysql_store_result(conn);
					row = mysql_fetch_row(res);
					chave = atoi(row[0]);
					

					if(vetor[2] == 0){ //apenas 1 dia reservado na semana 
						reserva_BD(vetor[7], vetor[8],vetor[9],vetor[10],vetor[0],vetor[5],vetor[1],vetor[19], motivo, chave);
						if(vetor[4] != 0 && vetor[6] != 0){reserva_rec(vetor[7], vetor[8],vetor[9],vetor[10],vetor[0],vetor[5],vetor[1],vetor[19], motivo, chave);}
					}

					if(vetor[2] != 0 && vetor[3] == 0){ //dois dias reservados na semana
						reserva_BD(vetor[7], vetor[8],vetor[9],vetor[10],vetor[0],vetor[5],vetor[1],vetor[19], motivo, chave);
						if(vetor[4] != 0 && vetor[6] != 0){reserva_rec(vetor[7], vetor[8],vetor[9],vetor[10],vetor[0],vetor[5],vetor[1],vetor[19], motivo, chave);}
						reserva_BD(vetor[11], vetor[12],vetor[13],vetor[14],vetor[0],vetor[5],vetor[2],vetor[19], motivo, chave);
						if(vetor[4] != 0 && vetor[6] != 0){reserva_rec(vetor[11], vetor[12],vetor[13],vetor[14],vetor[0],vetor[5],vetor[2],vetor[19], motivo, chave);}
					}
					
					if(vetor[3] != 0){ //tres dias reservados na semana
						reserva_BD(vetor[7], vetor[8],vetor[9],vetor[10],vetor[0],vetor[5],vetor[1],vetor[19], motivo, chave);
						if(vetor[4] != 0 && vetor[6] != 0){reserva_rec(vetor[7], vetor[8],vetor[9],vetor[10],vetor[0],vetor[5],vetor[1],vetor[19], motivo, chave);}
						reserva_BD(vetor[11], vetor[12],vetor[13],vetor[14],vetor[0],vetor[5],vetor[2],vetor[19], motivo, chave);
						if(vetor[4] != 0 && vetor[6] != 0){reserva_rec(vetor[11], vetor[12],vetor[13],vetor[14],vetor[0],vetor[5],vetor[2],vetor[19], motivo, chave);}
						reserva_BD(vetor[15], vetor[16],vetor[17],vetor[18],vetor[0],vetor[5],vetor[3],vetor[19], motivo, chave);
						if(vetor[4] != 0 && vetor[6] != 0){reserva_rec(vetor[15], vetor[16],vetor[17],vetor[18],vetor[0],vetor[5],vetor[3],vetor[19], motivo, chave);}
					}
				}
			}
		}
		else
		{
			cout << "\nAlunos nao podem fazer reservas!" << endl;
			getchar();
			getchar();
		}

	}
	else{cout<<	"cpf nao cadastrado!"<< endl;}

	chaves.clear();
}

void Banco::reserva_BD(int he, int hs, int me, int ms, int ano, int mes, int dia, int sala, string motivo, int ref){
	sprintf(comando, "insert into reservas(he,hs,data,sala,motivo,ref_aux) values ('%d:%d', '%d:%d', '%d-%d-%d','%d', '%s', '%d');", he, me, hs, ms, ano, mes, dia , sala, motivo.c_str(), ref);
	mysql_query( conn, comando );
}

void Banco::reserva_rec(int he, int hs, int me, int ms, int ano, int mes, int dia, int sala, string motivo, int ref){
	bool x = true;

	string data_fim;

	if(vetor[6] > 9 && vetor[4] > 9){	
		sprintf(comando, "%d-%d-%d",vetor[0], vetor[6], vetor[4]);
		data_fim = comando;
	}

	//controle de datas para o mysql, adicionando o 0 antes dos inteiros 
	if(vetor[6] < 10){
		if(vetor[4] < 10){
			sprintf(comando, "%d-0%d-0%d",vetor[0], vetor[6], vetor[4]);
		}
		else{
			sprintf(comando, "%d-0%d-%d",vetor[0], vetor[6], vetor[4]);
		}
		 data_fim = comando;
	}
	if(vetor[6] > 10 && vetor[4] < 10){
		sprintf(comando, "%d-%d-0%d",vetor[0], vetor[6], vetor[4]);
		data_fim = comando;
	}
	

	sprintf(comando, "select date_add('%d-%d-%d', interval 7 DAY);", ano, mes, dia);
	mysql_query( conn, comando );
	res= mysql_store_result(conn);
	row = mysql_fetch_row(res);
	string ab = row[0];	
	
	cin.ignore();

	while(x == true){
		sprintf(comando, "select if('%s' <= '%s', '1','0');", ab.c_str(), data_fim.c_str()); // row[0] vai ser 1 se a data ainda for válida
		mysql_query( conn, comando );
		res= mysql_store_result(conn);
		row = mysql_fetch_row(res);
		string s = row[0];
		if(s == "1"){
			sprintf(comando, "insert into reservas(he,hs,data,sala,motivo,ref_aux) values ('%d:%d', '%d:%d', '%s','%d', '%s', '%d');", he, me, hs, ms, ab.c_str(), sala, motivo.c_str(), ref);
			mysql_query( conn, comando );
		}
		else{
			x = false;
		}

		sprintf(comando, "select date_add('%s', interval 7 DAY);", ab.c_str());
		mysql_query( conn, comando );
		res= mysql_store_result(conn);
		row = mysql_fetch_row(res);
		ab = row[0];
	}

}

void Banco::addDiscToBD(Auxiliar *a, unsigned long int x){
	bool teacher;

	sprintf(comando, "select matricula from info where cpf = '%lu';", x);
	mysql_query( conn, comando );
	res= mysql_store_result(conn);
	row = mysql_fetch_row(res);

	if (row == NULL){
		cout << "Cpf nao cadastrado no banco de dados!" << endl;
		getchar();
		return;
	}

	if(row[0] != NULL){
		teacher = false;
	}
	else{
		teacher = true;
	}

	Turmas::iterator jt = a->turmas.begin();
	Disciplinas::iterator it ;

	for(it = a->disciplinas.begin(); it != a->disciplinas.end();it++){
	
			if(!teacher){//se for um aluno
				string p = *it;
				string q = *jt;
				sprintf(comando,"select t1.chave from auxtab t1 join disciplinas t2 on t1.FKdisc = t2.cod join turmas t3 on t1.FKturmas = t3.id join info t4 on t1.FKinfo = t4.cpf where t2.nome = '%s' and t3.nome = '%s' and t4.matricula is null;", p.c_str(), q.c_str());
				mysql_query( conn, comando );
				res= mysql_store_result(conn);
			    row = mysql_fetch_row(res);
			    if(row == NULL){
			    	cout << "ERRO: Disciplina / Turma nao encontrados no banco de dados!" << endl;
			    	cout << "Pressione alguma  tecla para continuar" << endl;
			    	getchar();
			    	return;
			    }

			    string aux = row[0];

			    //se tiver um cpf, ha um professor para a disciplina, portanto pode ser cadastrado
			    	sprintf(comando,"select FKdisc,FKturmas from auxtab where chave =  '%s' ", aux.c_str());
				    mysql_query( conn, comando );
				    res= mysql_store_result(conn);
			    	row = mysql_fetch_row(res);
			    	q = row[0];
			    	p = row[1];
			    	
			    	cout << "disc: " << q << endl;
			    	cout << "turm: " << p << endl;

					sprintf(comando, "insert into auxtab (FKinfo,FKturmas,FKdisc) values ('%lu','%s','%s');", x, p.c_str(), q.c_str());
					mysql_query( conn, comando );
			    

			}
			else{//se for professor
				sprintf(comando, "select * from info where cpf = '%lu';", x);
				mysql_query( conn, comando );
				res= mysql_store_result(conn);
				row = mysql_fetch_row(res);

				if(row == NULL){
					cout << "Cpf nao cadastrado no banco de dados!" << endl;
					getchar();
					return;
				}
				else{
				
					string p = *it;
					sprintf(comando, "insert into disciplinas(nome) values ('%s');", p.c_str());
					mysql_query( conn, comando );
			
					p = *jt;
					sprintf(comando, "insert into turmas(nome) values ('%s');", p.c_str());
					mysql_query( conn, comando );
				
					p = *it;
					sprintf(comando, "select cod from disciplinas where nome = '%s';", p.c_str());
					mysql_query( conn, comando );
					res= mysql_store_result(conn);
					row = mysql_fetch_row(res);
				
					string cod = row[0];
	
					p = *jt;
					sprintf(comando, "select id from turmas where nome = '%s';", p.c_str());
					mysql_query( conn, comando );
					res= mysql_store_result(conn);
					row = mysql_fetch_row(res);
					string id = row[0];

					sprintf(comando, "insert into auxtab(FKinfo,FKturmas,FKdisc) values ('%lu','%s','%s');", x, id.c_str(), cod.c_str());
					mysql_query( conn, comando );
				}
			}
		
			++jt;
	}
}

void Banco::find_way(unsigned long int x, bool teacher, bool externo){
	vector<string> chave_disc;
	vector<string> chave_turm;
	chave_disc.clear();
	chave_turm.clear();

	//pegando data do dia em que o usuario deseja acesso
	sprintf(comando, "select Day(now());");
	mysql_query( conn, comando );
	res = mysql_store_result(conn);
	row = mysql_fetch_row(res);
	string dia = row[0];

	sprintf(comando, "select Month(now());");
	mysql_query( conn, comando );
	res= mysql_store_result(conn);
	row = mysql_fetch_row(res);
	string mes = row[0];

	sprintf(comando, "select Year(now());");
	mysql_query( conn, comando );
	res= mysql_store_result(conn);
	row = mysql_fetch_row(res);
	string ano = row[0];
	
	sprintf(comando, "select matricula from info where cpf = '%lu';", x);
	mysql_query( conn, comando );
	res= mysql_store_result(conn);
	row = mysql_fetch_row(res);

	if(row == NULL && externo == false){
		cout << "Usuario nao cadastrado no banco de dados!!" << endl;
		cout << "Pressione qualquer tecla para continuar";
		getchar();
		getchar();
		return;
	}

	if(externo == true){ //caso seja um usuario sem cadastro no Linf
		int a;
		cout << "Digite o id da reserva: ";
		cin >> a;
		sprintf(comando, "select sala,he,hs,motivo from reservas where id_res = '%d';", a);
		mysql_query( conn, comando );
		res= mysql_store_result(conn);
		row = mysql_fetch_row(res);
		if (row == NULL){
			cout << "\nERRO: id de reserva invalido!!" << endl;
			cout << "Pressione qualquer tecla para continuar";
			getchar();
			getchar();
			return;
		}
		else{
			cout << "\nsala: " << row[0] << "\nHorario de entrada: " << row[1] << "\nHorario de saida: " << row[2] << "\nMotivo: " << row[3] << endl;
			cout << "Pressione qualquer tecla para continuar";
			getchar();
			getchar();
			return;
		}
	}
	
	if(row[0] != NULL){// se for aluno
		//Achando todas as chaves de matriculas do aluno

		sprintf(comando, "select FKdisc from auxtab where FKinfo = '%lu';", x);
		mysql_query( conn, comando );
		res= mysql_store_result(conn);
		while ((row = mysql_fetch_row(res)) != NULL ) 
		{ 
			for (int i =0; i < mysql_num_fields(res); i++){
				chave_disc.push_back(row[i]);
			}
		}

		sprintf(comando, "select FKturmas from auxtab where FKinfo = '%lu';", x);
		mysql_query( conn, comando );
		res= mysql_store_result(conn);
		while ((row = mysql_fetch_row(res)) != NULL ) 
		{ 
			for (int i =0; i < mysql_num_fields(res); i++){
				chave_turm.push_back(row[i]);
			}
		}

		int e = 0;
	    for(int i = 0; i < chave_disc.size(); i++){
	    	string s = chave_disc.at(i);
	    	string p = chave_turm.at(i);

			sprintf(comando, "select sala,he,hs,motivo from reservas where ref_aux = (select t2.chave from auxtab t2 join info t1 on t1.cpf = t2.FKinfo where t2.FKdisc = '%s' and t2.FKturmas = '%s' and t1.matricula is null) and data = '%s-%s-%s';", s.c_str(), p.c_str(), ano.c_str(),mes.c_str(),dia.c_str());
			mysql_query( conn, comando );
			res= mysql_store_result(conn);
			row = mysql_fetch_row(res);
			if (row == NULL){
				continue;
			}
			else{
				e++;
				cout << "sala: " << row[0] << "\nHorario de entrada: " << row[1] << "\nHorario de saida: " << row[2] << "\nMotivo: " << row[3] << endl;
			}
		}

		if(e == 0){
			cout << "nao existem reservas disponiveis para o cpf "<< x << " hoje" << endl;
		}
		e=0;
    }


    else{ //se for funcionario
    	if(teacher == true){//professor
	    	sprintf(comando, "select FKdisc from auxtab where FKinfo = '%lu';", x);
			mysql_query( conn, comando );
			res= mysql_store_result(conn);
			while ((row = mysql_fetch_row(res)) != NULL ) 
			{ 
				for (int i =0; i < mysql_num_fields(res); i++){
					chave_disc.push_back(row[i]);
				}
			}

			sprintf(comando, "select FKturmas from auxtab where FKinfo = '%lu';", x);
			mysql_query( conn, comando );
			res= mysql_store_result(conn);
			while ((row = mysql_fetch_row(res)) != NULL ) 
			{ 
				for (int i =0; i < mysql_num_fields(res); i++){
					chave_turm.push_back(row[i]);
				}
			}

			int e = 0;
		    for(int i = 0; i < chave_disc.size(); i++){
		    	string s = chave_disc.at(i);
		    	string p = chave_turm.at(i);
				sprintf(comando, "select sala,he,hs,motivo from reservas where ref_aux = (select t2.chave from auxtab t2 join info t1 on t1.cpf = t2.FKinfo where t2.FKdisc = '%s' and t2.FKturmas = '%s' and t1.matricula is null) and data = '%s-%s-%s';", s.c_str(), p.c_str(), ano.c_str(),mes.c_str(),dia.c_str());
				mysql_query( conn, comando );
				res= mysql_store_result(conn);
				row = mysql_fetch_row(res);
				if (row == NULL){
					continue;
				}
				else{
					e++;
					cout << "sala: " << row[0] << "\nHorario de entrada: " << row[1] << "\nHorario de saida: " << row[2] << "\nMotivo: " << row[3] << endl;
				}
			}

			if(e == 0){
				cout << "nao existem reservas disponiveis para o cpf "<< x << " hoje" << endl;
			}
			e=0;
	    }


    	else{//demais funcionarios
    		
    		sprintf(comando, "select chave from auxtab where FKinfo = '%lu';", x);
			mysql_query( conn, comando );
			res= mysql_store_result(conn);
			row = mysql_fetch_row(res);
			string s = row[0];
    		sprintf(comando, "select sala,he,hs,motivo from reservas where ref_aux = '%s' and data = '%s-%s-%s';", s.c_str(), ano.c_str(), mes.c_str(), dia.c_str());
    		mysql_query( conn, comando );
			res= mysql_store_result(conn);
			row = mysql_fetch_row(res);
			if (row == NULL){
				cout << "nao existem reservas disponiveis para o cpf "<< x << " hoje" << endl;
			}
			else{
				cout << "sala: " << row[0] << "\nHorario de entrada: " << row[1] << "\nHorario de saida: " << row[2] << "\nMotivo: " << row[3] << endl;
			}
		}
    }
    cout << "Pressione qualquer tecla para continuar";
	getchar();
	getchar();
}

void Banco::rmvDisc(unsigned long int x){
	int j,op, chave;
	vector<int> chaves;
	chaves.clear();
	bool teacher;

	sprintf(comando, "select matricula from info where cpf = '%lu';", x);
	mysql_query( conn, comando );
	res= mysql_store_result(conn);
	row = mysql_fetch_row(res);

	if(row == NULL){ 
		cout << "cpf não cadastrado no banco de dados!" << endl;
		return;
	}
	if(row[0] == NULL){
		teacher = true;
	}
	else{
		teacher = false;
	}

	sprintf(comando, "select t2.nome, t3.nome, t1.chave from auxtab t1 join disciplinas t2 on t1.FKdisc = t2.cod join turmas t3 on t1.FKturmas = t3.id where t1.FKinfo = '%lu';", x);
	mysql_query( conn, comando );
	res= mysql_store_result(conn);

	cout << "\ndisciplina / turma\n" << endl;
	int aux = mysql_num_fields(res) ;
	j= 1;

	while ((row = mysql_fetch_row(res)) != NULL ) 
	{ 	
		for (int i=0; i<aux; i++) {
			if(i+1 == aux){ //sempre que chegar no ultimo 'i' será uma chave da tabela auxtab
				int a = atoi(row[i]); //converte o vetor de char para um int
				chaves.push_back(a); //essa chave vai ser salva no vetor de chaves
			}
			else{
				cout << row[i] << "   " ;
			}
		}
				
		cout << "    opcao [" << j << "]" <<endl;
		j++;
	} 

	cout << "digite a opcao desejada : ";
	cin >> op;

	chave = chaves.at(op-1);

	sprintf(comando, "set foreign_key_checks = 0;");
	mysql_query( conn, comando );

	sprintf(comando, "delete FROM auxtab where chave = '%d';", chave);
	mysql_query( conn, comando );

	if(teacher){
		sprintf(comando, "delete FROM reservas where ref_aux = '%d';", chave);
		mysql_query( conn, comando );
	}
	
	sprintf(comando, "set foreign_key_checks = 1;");
	mysql_query( conn, comando );
}
