# Introdução

O seguinte projeto foi elaborado com a intenção de criar um sistema de acesso ao Linf, laboratório de informática da UnB. O sistema permite que usuários se cadastrem, adicionem disciplinas (alunos ou professores), façam reservas no laboratório ou excluam cadastros. As únicas entidades que terão uma matrícula serão os alunos, os quais devem informá-la ao cadastrar-se no banco de dados. Todos os usuários serão identificados pelo cpf para realizar qualquer ação.<br>
O sistema é integrado ao banco de dados [MySQL](https://www.mysql.com/), garantindo que todos os registros sejam armazenados e a biblioteca OpenCV para tirar fotos e realizar detecção facial.

## OpenCV

OpenCV (Open Source Computer Vision Library). Originalmente, desenvolvida pela Intel, em 2000, é uma biblioteca multiplataforma, totalmente livre ao uso acadêmico e comercial, para o desenvolvimento de aplicativos na área de Visão computacional, bastando seguir o modelo de licença BSD Intel. 
O OpenCV possui módulos de Processamento de Imagens e Video I/O, Estrutura de dados, Álgebra Linear, GUI (Interface Gráfica do Usuário) Básica com sistema de janelas independentes, Controle de mouse e teclado, além de mais de 350 algoritmos de Visão computacional como: Filtros de imagem, calibração de câmera, reconhecimento de objetos, análise estrutural e outros. 
O seu processamento é em tempo real de imagens. Esta biblioteca foi desenvolvida nas linguagens de programação C/C++. Também, dá suporte a programadores que utilizem Java, Python e Visual Basic e desejam incorporar a biblioteca a seus aplicativos. A versão 1.0 foi lançada no final de 2006 e a 2.0 foi lançada em setembro de 2009.
O obejteio do OpenCV no trabalho sis_acesso_cic era de adquirir fotos do rosto do usuário no momento de cadastro e armazena-las, como mostra a imagem a seguir: <br>

## MySQL

O MySQL é um sistema gerenciador de banco de dados relacional de código aberto usado na maioria das aplicações gratuitas para gerir suas bases de dados. O serviço utiliza a linguagem SQL (Structure Query Language – Linguagem de Consulta Estruturada), que é a linguagem mais popular para inserir, acessar e gerenciar o conteúdo armazenado num banco de dados.

# Requisitos

 - `Bibliotecas:`
  
    **#include< iostream >**

    **#include < stdio.h >**
    
    **#include < vector >**
    
    **#include < stdlib.h >**
    
    **#include < mysql/mysql.h >**
    
    **#include "opencv2/objdetect/objdetect.hpp"**
    
    **#include "opencv2/highgui/highgui.hpp"**
    
    **#include "opencv2/imgproc/imgproc.hpp"**

 - `C++ / Compilador:`
 
    versão usada: **gcc version 5.4.0**
    
 - `Ajuste:`
    
    Antes de gerar a linha de comando, será necessário fazer uma breve mudança no arquivo `banco.cpp`.
    Vá até o arquivo, nas linhas 5-8. Lá estão definidas algumas configurações para acesso ao banco.
    Preencha-as com seus dados e salve o arquivo.
    

# Linha de Comando

> Necessita usar-se o arquivo Makefile presente no repositório.
>
> $sudo apt-get install libmysqlclient-dev //Comando necessário para a biblioteca do mysql  
>
> ($make all)  ou  ($make trab2)
>
> $./trab2

Visualização do menu principal para o usuário:
![Menu](/img/menu.png)

# Arquivos  

> - `usuario.h`
>
>    Protótipo das funções que serão implementadas no arquivo **usuario.cpp**
>
> - `usuario.cpp`
>
>    Implementação das funções do arquivo **usuario.h**. Essa classe é responsável pela identificação,
>    contendo os atributos e métodos básicos para todos os usuários.
>
> - `funcionarios.h`
>
>    Protótipo das funções que serão implementadas no arquivo **funcionarios.cpp**
>
> - `funcionarios.cpp`
>
>    Implementação das funções do arquivo **funcionarios.h**. Essa classe é responsável pelas reservas,
>    que podem ser feitas apenas por funcionarios.
>
> - `aluno.h`
>
>    Protótipo das funções que serão implementadas no arquivo **aluno.cpp**
>
> - `aluno.cpp`
>
>    Implementação das funções do arquivo **aluno.h**. Essa classe é responsável por cuidas
>    das matrículas dos alunos.
>
> - `banco.h`
>
>    Protótipo das funções que serão implementadas no arquivo **banco.cpp**. É nesse arquivo que são incluídas 
>    todas as bibliotecas necessárias no projeto.
>
> - `banco.cpp`
>
>    Implementação das funções do arquivo **banco.h**. Essa classe é responsável pela integração de todo o
>    projeto com o banco de dados.
>
> - `auxiliar.h`
>
>    Protótipo das funções que serão implementadas no arquivo **auxiliar.cpp**.
>
> - `auxiliar.cpp`
>
>    Implementação das funções do arquivo **auxiliar.h**. Essa classe tem o intuito de armazenar as disciplinas
>    de alunos e professores.
>
> - `haarcascade_frontalface_alt.xml`
>
>   Arquivo necessário para fazer a detecção facial utilizado pelo OpenCV.
>
> - `LinfRev.sql`
>
>   Arquivo para criação do banco de dados de forma automática.
>
> - `Makefile`<br>
>   Arquivo que agrupa os requisítos de compilação, após a instalação dos componentes necessários.
>
><br>
> - `Doxygen`
>
>   Arquivo necessário para criar a documentação do diagrama de classes com detalhamento de cada uma das classes usadas.
>

# Instalação

Para que o projeto funcione corretamente, devemos preparar o ambiente da seguinte forma :

## Instalação do MySQL

Digite o comando:<br>
```sudo apt-get install mysql-server mysql-client```<br>
Siga com a instalação normalmente, até aparecer esta janela, onde você escolhe a senha do seu root no MySQL:
![senha1](/img/prim.png)
Confirme sua senha na janela seguinte:
![senha2](/img/seg.png)
Após a instalação ter sido concluída, digite o comando:<br>
```mysql -u root -p```<br>
Logo após, digite a senha para seu root (definida acima) e pressione enter.
![entrando](/img/terc.png)
Agora, você está conectado diretamente com o MySQL, e deverá criar um Banco de dados com as tabelas necessárias.
Para isso, basta copiar todo o texto que está no arquivo LinfRev.sql (caso necessário, abra com o editor de texto da sua máquina) e colar no   terminal. Depois, pressione ENTER.
Após a execução acima, teremos o banco modelado abaixo:
![banco](/img/Linf.png)

<strong>legenda:</strong><br>
<strong> chave </strong> : são as primary keys das tabelas, isso significa que não podem haver registros com a mesma primary key nas tabelas, sendo assim elementos únicos.<br>
<strong> losangos escuros / claros </strong> : representam a obrigatoriedade do dado, os escuros representam os obrigatórios e os claros os não obrigatórios<br>
<strong> losangos rosas / azuis </strong> : losangos rosas são as foreign keys das tabelas, que são chaves que servem para relacionar as tabelas. Já os losangos azuis, representam os atributos simples das tabelas<br>
<strong> notação de cardinalidade </strong> : um e somente um, se relaciona com um ou vários. As tabelas info, disciplinas e turmas tem um relacionamento de um e somente um para um ou vários com a tabela auxtab. A tabela auxtab, por sua vez, tem um relacionamento de um e somente um para um ou váŕios com a tabela reservas.<br>

## Instalação do OpenCV

Para instalação do OpenCV deve ser seguido o script no link: https://github.com/brunojus/bash-opencv<br>
<strongFirst Step</strong><br>
Clone this project
```git clone https://github.com/brunojus/bash-opencv.git```<br>
<strong>Second Step</strong><br>
```cd bash-opencv/```<br>
```sudo chmod +x opencv.sh```<br>
```sudo ./opencv.sh```<br>
<strong>Third Step</strong><br>
Enter this in terminal<br>
1)```pkg-config --cflags opencv```<br>
You probably see this<br>
```-I/usr/local/include/opencv -I/usr/local/include```<br>
2)```pkg-config --libs opencv```<br>
You probably this in terminal<br>
```-L/usr/local/lib -lopencv_shape -lopencv_stitching -lopencv_objdetect -lopencv_superres -lopencv_videostab -lopencv_calib3d -lopencv_features2d -lopencv_highgui -lopencv_videoio -lopencv_imgcodecs -lopencv_video -lopencv_photo -lopencv_ml -lopencv_imgproc -lopencv_flann -lopencv_core```<br>
To build all C++ program<br>
1) ```g++ programa.cpp -o program `pkg-config --cflags --libs opencv` ```<br>
2)```$./program```<br>
To build this tutorial present in repository.<br>
1) ```$g++ test_opencv.cpp -o name `pkg-config --cflags --libs opencv` ```<br>
2) ./name HappyFish.jpg<br>

<strong>DEMOSTRAÇÃO DO OPENCV FAZENDO A DETECÇÃO FACIAL:<br></strong>
![Programa em execução](/img/quarta.png)
<strong>DEMOSTRAÇÃO DE FOTOS TIRADAS COM A BIBLIOTECA OPENCV:<br></strong>
![Fotos salvas](/img/openCV.jpeg)

## Instalação do Doxygen

Para instalar o Doxygen é necessário digitar o comando:
```sudo apt-get install doxygen```



# Doxygen
O Doxygen é um programa que gera a documentação de um programa a partir da análise de um código
escrito em C (ou linguagens derivadas como C++, Java ou C#). Nesses códigos são reconhecidas
estruturas de dados, funções e comentários feitos com uma sintaxe especial.

## Instalação do Graphviz

Para gerar a documentação com a parte visual das classes e relacionamentos é necessário instalar o Graphviz, um software para visualização gráfica:
Comando para instalar:
```sudo apt-get install graphviz```

Para gerar a documentação digite dentro da pasta:
```doxygen -g```

Após isso será necessário mudar algumas variáveis habilitando as seguintes funções no arquivo Doxyfile:
    
    EXTRACT_ALL            = YES
    HAVE_DOT               = YES
    UML_LOOK               = YES
    
E após isso executar o comando:
```doxygen -g```

[Documentação Doxygen](https://glcdn.githack.com/andrehct/sis_acesso_cic/raw/master/html/index.html)

# Makefile

O objetivo de Makefile é definir regras de compilação para projetos de software. Tais regras são definidas em um arquivo chamado Makefile. O programa make interpreta o conteúdo do Makefile e executa as regras lá definidas. Para gerar o makefile usamos o CMake, que é um sistema multiplataforma para geração automatizada. É comparável com o programa Unix Make no qual o processo de geração é, ao final, controlado pelos arquivos de configuração, no caso do CMake chamados de arquivos CMakeLists.txt. Diferente de Make, ele não gera diretamente o software final, mas em vez disso gera arquivos de geração padrões como por exemplo o makefile.
Acabamos tendo que criar o <strong>Makefile</strong> na mão, por problemas de integração do <strong>Mysql</strong> com o <strong>CMake</strong>, com os comandos abaixo em negrito:<br>
<strong>all: trab2<br>
trab2: banco.cpp trab2.cpp usuario.cpp aluno.cpp funcionarios.cpp auxiliar.cpp<br> 
CXX=g++<br>
CPPFLAGS = $(shell pkg-config --cflags opencv)<br>
LDLIBS=-std=c++11 -L/usr/lib/mysql -lmysqlclient $(shell pkg-config --libs opencv)</strong>

# Diagramas de Sequência

O diagrama de sequência mostra uma interação, que representa a sequência de mensagens entre instâncias de classes, componentes, subsistemas ou atores. Nesse diagrama vamos mostrar como funciona o programa em si, mostrando sua funcionalidade de cadastro por exemplo, e outras finalidade também.<br>
<strong>Diagrama - [Cadastro de Funcionário:](Cadastro de Funcionário:)</strong>
![Diagrama](/img/funcionario_diag.jpeg)
<strong>Diagrama - [Cadastro de Aluno:](Cadastro de Aluno:)</strong>
![Diagrama](/img/aluno_diag.jpeg)
<strong>Diagrama - [Acesso do Aluno ao Linf:](Acesso do Aluno ao Linf:)</strong>
![Diagrama](/img/Acesso_Aluno.jpeg)