#ifndef USUARIO_H
#define USUARIO_H
#include<iostream>
#include<string.h>

using namespace std;

class Usuario{

protected:
	string nome;
	unsigned long int cpf;
public:
	void setNome(string s);
	string getNome();
	void setCpf(unsigned long int x);
	unsigned long int getCpf();
};

#endif
