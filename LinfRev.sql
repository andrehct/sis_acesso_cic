-- MySQL Script generated by MySQL Workbench
-- Sáb 25 Nov 2017 19:30:25 -02
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema Linf
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema Linf
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `Linf` DEFAULT CHARACTER SET utf8 ;
USE `Linf` ;

-- -----------------------------------------------------
-- Table `Linf`.`info`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Linf`.`info` (
  `cpf` BIGINT(20) NOT NULL,
  `matricula` BIGINT(20) NULL DEFAULT NULL,
  `nome` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`cpf`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `Linf`.`turmas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Linf`.`turmas` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(5) NOT NULL,
  PRIMARY KEY (`id`, `nome`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `Linf`.`disciplinas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Linf`.`disciplinas` (
  `cod` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(40) NOT NULL,
  PRIMARY KEY (`cod`, `nome`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `Linf`.`auxtab`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Linf`.`auxtab` (
  `chave` INT(11) NOT NULL AUTO_INCREMENT,
  `FKinfo` BIGINT(20) NOT NULL,
  `FKturmas` INT(11) NULL DEFAULT NULL,
  `FKdisc` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`chave`),
  INDEX `FKinfo` (`FKinfo` ASC),
  INDEX `FKturmas` (`FKturmas` ASC),
  INDEX `FKdisc` (`FKdisc` ASC),
  CONSTRAINT `auxtab_ibfk_1`
    FOREIGN KEY (`FKinfo`)
    REFERENCES `Linf`.`info` (`cpf`),
  CONSTRAINT `auxtab_ibfk_2`
    FOREIGN KEY (`FKturmas`)
    REFERENCES `Linf`.`turmas` (`id`),
  CONSTRAINT `auxtab_ibfk_3`
    FOREIGN KEY (`FKdisc`)
    REFERENCES `Linf`.`disciplinas` (`cod`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `Linf`.`reservas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Linf`.`reservas` (
  `id_res` INT(11) NOT NULL AUTO_INCREMENT,
  `he` TIME NOT NULL,
  `hs` TIME NOT NULL,
  `data` DATE NULL DEFAULT NULL,
  `motivo` VARCHAR(100) NULL DEFAULT NULL,
  `sala` TINYINT(4) NOT NULL,
  `ref_aux` INT(11) NOT NULL,
  PRIMARY KEY (`id_res`),
  INDEX `ref_aux` (`ref_aux` ASC),
  CONSTRAINT `reservas_ibfk_1`
    FOREIGN KEY (`ref_aux`)
    REFERENCES `Linf`.`auxtab` (`chave`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
