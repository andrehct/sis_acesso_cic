#ifndef FUNCIONARIOS_H
#define FUNCIONARIOS_H

#include <vector>
#include "usuario.h"

class Funcionarios : public Usuario{
public:
	void fazer_reserva(int sala, bool func);
	typedef vector<unsigned long int> Participantes;
	Participantes participantes;
	
private:
	int opc=0, ano= 0, d1= 0, d2= 0, d3= 0, df= 0, mes_ini= 0, mes_fim= 0, he = 0, hs= 0;
	int me= 0, ms= 0, he2= 0, hs2= 0, me2= 0, ms2= 0, he3= 0, hs3= 0, me3= 0, ms3= 0;
};

#endif