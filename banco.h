#ifndef BANCO_H
#define BANCO_H


//Biblioteca necessária para conexao com o mysql
#include <mysql/mysql.h>
#include <stdio.h>

#include <vector>
#include <stdlib.h> //precisa para converter vetores de char em int
#include <iostream>
#include "usuario.h"
#include "aluno.h"
#include "funcionarios.h"
#include "auxiliar.h"

using namespace std;


class Banco : public Auxiliar, public Funcionarios{
private:
	char comando[300];
	MYSQL *conn = mysql_init(NULL);
	MYSQL_RES *res;
	MYSQL_ROW row;
	void reserva_BD(int he, int hs, int me, int ms, int ano, int mes, int dia, int sala, string motivo, int ref); //funcao que adiciona uma reseva ao banco de dados
    void reserva_rec(int he, int hs, int me, int ms, int ano, int mes, int dia, int sala, string motivo, int ref); //funcao que vai adicionar varias reservas ao banco de dados (recorrente)
public:
    Banco();
    void start(); //funcao que inicia a conexao com o banco de dados
    void close(); //funcao que encerra a conexao com o banco de dados
    void addToBD(Aluno *a); //funcao que adiciona um aluno ao banco de dados
    void addToBD(Funcionarios *f); //funcao que adiciona um funcionario ao banco de dados
    void addDiscToBD(Auxiliar *a, unsigned long int x); //funcao que adiciona as disciplinas e turmas ao banco de dados
    void rmvFromBD(unsigned long int x); //funcao que remove um usuario do banco de dados
    void reserva(unsigned long int x, bool teacher, string motivo, Funcionarios *f); //funcao que valida o cpf e coleta alguns dados da reserva
    void find_way(unsigned long int x, bool teacher, bool externo); //funcao que acha a sala, horario, data e motivo da resera
    void rmvDisc(unsigned long int x); // funcao que remove uma disciplina do banco de dados
};


#endif
